Shaylo Records (formerly known as Shaylo Family) is an American record label founded by record producer Shay Legacy. The label focuses on bringing artist from all over the world regardless of genres. 

# Connect with Shaylo Records
https://www.instagram.com/shaylorecords  
https://soundcloud.com/shaylorecords  
https://twitter.com/ShayloRecords  
https://www.facebook.com/ShayloReocrds  
